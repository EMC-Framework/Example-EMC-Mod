EMC (Easy Minecraft Client) Example Mod
===================

Example mod for [EMC](https://gitlab.com/EMC-Framework/EMC)

For more help visit the [EMC Development Kit](https://gitlab.com/EMC-Framework/EMC)

You can find API documentation at the [EMC website](https://emc-framework.gitlab.io/)

Setting up your IDE
-------------------

1. Import the project into your IDE
2. Run `mappings.sh` or `mappings.bat` (depending on your OS).
3. Refresh gradle

Installing the compiled mod
-------------------

Once you're done with your mod, run the gradle `build` task. Copy your mod jar file from `build/libs/` to `.minecraft/libraries/EMC/<Minecraft version>/` and start Minecraft with EMC (See below on how to start Minecraft with EMC).

Running EMC in the Minecraft launcher
-------------------

1. Download the [EMC 1.15.1](https://gitlab.com/EMC-Framework/EDK/1.15.1-EMC/) dir and put it in `.minecraft/versions/`
2. Create a new profile and select `release 1.15.1-EMC`
3. Start Minecraft

License
-------------------

EMC is licensed under MIT

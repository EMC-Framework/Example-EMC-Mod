package me.deftware.mod;

import com.mojang.brigadier.builder.LiteralArgumentBuilder;
import me.deftware.client.framework.command.CommandBuilder;
import me.deftware.client.framework.command.CommandRegister;
import me.deftware.client.framework.command.EMCModCommand;
import me.deftware.client.framework.event.EventBus;
import me.deftware.client.framework.event.EventHandler;
import me.deftware.client.framework.event.events.EventUpdate;
import me.deftware.client.framework.wrappers.IChat;
import me.deftware.client.framework.main.EMCMod;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main extends EMCMod {

	public static final Logger LOGGER = LogManager.getLogger("TestMod");
	public static Main INSTANCE;

	@Override
	public void initialize() {
		// Set the INSTANCE to this, so you can call Main.INSTANCE to get EMCMod functions
		// for your mod, such as Main.INSTANCE.getSettings()
		INSTANCE = this;
		// Here you initialize anything needed for the mod
		// We need to register this class to handle events
		// You can also create a new class and extend it by EventListener
		// to have EMC automatically register and pass events to it
		EventBus.registerClass(this.getClass(), this);
		// We can also register a custom client command here
		CommandRegister.registerCommand(new ExampleCommand());
	}

	@Override
	public void postInit() {
		// This method is called after Minecraft has been fully initialized
		// Here you can call functions such as IMinecraft.setIGuiSCreen
		LOGGER.info("TestMod active!");
	}

	@EventHandler(eventType = EventUpdate.class)
	public void onUpdate(EventUpdate event) {
		// Example of an update event, called 20 times a second
	}

	public class ExampleCommand extends EMCModCommand {

		@Override
		public CommandBuilder getCommandBuilder() {
			// For more info on how to use commands, look up the documentation for Mojang Brigadier
			return new CommandBuilder().set((LiteralArgumentBuilder) LiteralArgumentBuilder.literal("test")
					.executes(c -> {
						IChat.sendClientMessage("It works!");
						return 1;
					})
			);
		}

	}

}
